import {MigrationInterface, QueryRunner} from "typeorm";

export class FixRelationsAndAddEnums1629820496872 implements MigrationInterface {
    name = 'FixRelationsAndAddEnums1629820496872'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP CONSTRAINT "FK_85abcef0e8eb4e16b6767ada365"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "serviceArea"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "stageProbability"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "status"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "sellerId"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "serviceLine" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "prrId" uuid`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "stage" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "probability" integer NOT NULL`);
        await queryRunner.query(`CREATE TYPE "public"."prr_resolutionstatus_enum" AS ENUM('accepted', 'rejected')`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "resolutionStatus" "public"."prr_resolutionstatus_enum" NOT NULL`);
        await queryRunner.query(`CREATE TYPE "public"."prr_statestatus_enum" AS ENUM('submitted', 'reviewed')`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "stateStatus" "public"."prr_statestatus_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "submitterId" uuid`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "reviewerId" uuid`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "internalRole"`);
        await queryRunner.query(`CREATE TYPE "public"."resource_internalrole_enum" AS ENUM('associate', 'associateEngineer', 'engineer', 'seniorEnginner', 'principalEngineer')`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "internalRole" "public"."resource_internalrole_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD CONSTRAINT "FK_7c3b39b1ed71ba2b9439bf2876b" FOREIGN KEY ("prrId") REFERENCES "prr"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD CONSTRAINT "FK_f25429d1ec0379ac3a6bb32e9ee" FOREIGN KEY ("submitterId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD CONSTRAINT "FK_b99434934ee6516604b602beeca" FOREIGN KEY ("reviewerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP CONSTRAINT "FK_b99434934ee6516604b602beeca"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP CONSTRAINT "FK_f25429d1ec0379ac3a6bb32e9ee"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP CONSTRAINT "FK_7c3b39b1ed71ba2b9439bf2876b"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "internalRole"`);
        await queryRunner.query(`DROP TYPE "public"."resource_internalrole_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "internalRole" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "reviewerId"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "submitterId"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "stateStatus"`);
        await queryRunner.query(`DROP TYPE "public"."prr_statestatus_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "resolutionStatus"`);
        await queryRunner.query(`DROP TYPE "public"."prr_resolutionstatus_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "probability"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP COLUMN "stage"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "prrId"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "sellerId" uuid`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "status" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD "stageProbability" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "serviceArea" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD CONSTRAINT "FK_85abcef0e8eb4e16b6767ada365" FOREIGN KEY ("sellerId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
