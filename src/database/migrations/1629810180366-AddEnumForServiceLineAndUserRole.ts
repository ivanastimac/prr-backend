import {MigrationInterface, QueryRunner} from "typeorm";

export class AddEnumForServiceLineAndUserRole1629810180366 implements MigrationInterface {
    name = 'AddEnumForServiceLineAndUserRole1629810180366'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`CREATE TYPE "public"."user_serviceline_enum" AS ENUM('ADD', 'MS', 'DA')`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "serviceLine" "public"."user_serviceline_enum" NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "role"`);
        await queryRunner.query(`CREATE TYPE "public"."user_role_enum" AS ENUM('seller', 'admin')`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "role" "public"."user_role_enum" NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "role"`);
        await queryRunner.query(`DROP TYPE "public"."user_role_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "role" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."user" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`DROP TYPE "public"."user_serviceline_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."user" ADD "serviceLine" character varying NOT NULL`);
    }

}
