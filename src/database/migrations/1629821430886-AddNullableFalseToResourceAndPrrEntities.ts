import {MigrationInterface, QueryRunner} from "typeorm";

export class AddNullableFalseToResourceAndPrrEntities1629821430886 implements MigrationInterface {
    name = 'AddNullableFalseToResourceAndPrrEntities1629821430886'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP CONSTRAINT "FK_7c3b39b1ed71ba2b9439bf2876b"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ALTER COLUMN "prrId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP CONSTRAINT "FK_f25429d1ec0379ac3a6bb32e9ee"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "submitterId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD CONSTRAINT "FK_7c3b39b1ed71ba2b9439bf2876b" FOREIGN KEY ("prrId") REFERENCES "prr"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD CONSTRAINT "FK_f25429d1ec0379ac3a6bb32e9ee" FOREIGN KEY ("submitterId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" DROP CONSTRAINT "FK_f25429d1ec0379ac3a6bb32e9ee"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP CONSTRAINT "FK_7c3b39b1ed71ba2b9439bf2876b"`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "submitterId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ADD CONSTRAINT "FK_f25429d1ec0379ac3a6bb32e9ee" FOREIGN KEY ("submitterId") REFERENCES "user"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ALTER COLUMN "prrId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD CONSTRAINT "FK_7c3b39b1ed71ba2b9439bf2876b" FOREIGN KEY ("prrId") REFERENCES "prr"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
