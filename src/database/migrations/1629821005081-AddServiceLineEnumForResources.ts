import {MigrationInterface, QueryRunner} from "typeorm";

export class AddServiceLineEnumForResources1629821005081 implements MigrationInterface {
    name = 'AddServiceLineEnumForResources1629821005081'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`CREATE TYPE "public"."resource_serviceline_enum" AS ENUM('ADD', 'MS', 'DA')`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "serviceLine" "public"."resource_serviceline_enum" NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."resource" DROP COLUMN "serviceLine"`);
        await queryRunner.query(`DROP TYPE "public"."resource_serviceline_enum"`);
        await queryRunner.query(`ALTER TABLE "public"."resource" ADD "serviceLine" character varying NOT NULL`);
    }

}
