import {MigrationInterface, QueryRunner} from "typeorm";

export class AddNullableAndDefaultPropertiesToPrrEntity1630066998863 implements MigrationInterface {
    name = 'AddNullableAndDefaultPropertiesToPrrEntity1630066998863'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "resolutionStatus" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "stateStatus" SET DEFAULT 'Submitted'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "stateStatus" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "public"."prr" ALTER COLUMN "resolutionStatus" SET NOT NULL`);
    }

}
