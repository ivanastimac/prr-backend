import { getRepository } from "typeorm";
import { PrrEntity } from "../database/entities/PrrEntity";
import loggedIn from "../middleware/loggedIn";
import { ICreatePrrInput, IDeletePrrInput, IGetPrrInput, IUpdatePrrInput } from "../schema/prrSchema";
import { ICreateResourceInput, IDeleteResourceInput, IGetResourcesInput, IResource } from "../schema/resourceSchema";
import { IGetUserInput } from "../schema/userSchema";
import resourceResolver from "./resourceResolver";
import userResolver, { IContextUser } from "./userResolver";


const prrResolver = {

    Query: {

        // if logged in user is admin
        getAllPrrs: async (_: unknown, args: unknown, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't see all PRRs!");
            }

            const prrs = await getRepository(PrrEntity).find();

            return prrs;
        },

        // if logged in user is seller
        getPrrsSubmitter: async (_: unknown, args: unknown, context: IContextUser) => {

            loggedIn(context);

            const submitterId: IGetUserInput = {
                filter: {
                    id: context.user.id,
                }
            };

            const submitter = await userResolver.Query.getUser("", submitterId, context);

            const prrs = await getRepository(PrrEntity).find({
                where: {
                    submitter: submitter,
                }
            });
            
            return prrs;

        },

        getPrr: async (_: unknown, args: IGetPrrInput, context: IContextUser) => {

            loggedIn(context);

            const prr = await getRepository(PrrEntity).findOneOrFail({
                where: {
                    id: args.filter.id,
                }
            }).catch(() => {
                throw new Error("Prr not found!");
            });

            const prrId: IGetResourcesInput = {
                filter: {
                    prrId: args.filter.id,
                }
            };

            // get all resources from current prr
            const resources = await resourceResolver.Query.getResources("", prrId, context);

            // return object which contains prr's information and all resources from current prr
            return {
                ...prr,
                 resources
            };
        },

        getReviewerForPrr: async (_: unknown, args: IGetPrrInput, context: IContextUser) => {

            loggedIn(context);

            const prr = await getRepository(PrrEntity).findOneOrFail({
                where: {
                    id: args.filter.id,
                },
                relations: ["reviewer"]
            }).catch(() => {
                throw new Error("Prr not found!");
            });

           return prr.reviewer;
        }

    },

    Mutation: {

        createPrr: async (_: unknown, args: ICreatePrrInput, context: IContextUser) => {

            loggedIn(context);

            const submitterId: IGetUserInput = {
                filter: {
                    id: context.user.id,
                }
            };

            const submitter = await userResolver.Query.getUser("", submitterId, context);
        
            const newPrr = await getRepository(PrrEntity).create({
                submitter: submitter,
                accountName: args.input.accountName,
                accountOwner: args.input.accountOwner,
                projectName: args.input.projectName,
                projectDescription: args.input.projectDescription,
                stage: args.input.stage,
                probability: args.input.probability,
                resolutionStatus: "NotReviewed",
            });
            await newPrr.save();

            let resourcesArr: IResource[] = [];

            const resources = args.input.resources;

            // save all resources to db and to array 
            for (let i = 0; i < resources.length; ++i){
                
                // for each resource create new object with contains prr's id and resource's information
                const resource: ICreateResourceInput = {
                    input: {
                        prrId: newPrr.id,
                        ...resources[i],
                    }
                }

                const newResource = await resourceResolver.Mutation.createResource("", resource, context);
                resourcesArr.push(newResource);
            }

            return {
                ...newPrr,
                 resources: resourcesArr
            };
        },

        updatePrr: async (_: unknown, args: IUpdatePrrInput, context: IContextUser) => {

            loggedIn(context);

            if (!args.input.accountName && !args.input.accountOwner && !args.input.projectName && !args.input.projectDescription && !args.input.stage && !args.input.probability && !args.input.resolutionStatus) {
                throw new Error("You must specify at least one property to update prr's information!");
            }

            const prr = await getRepository(PrrEntity).findOneOrFail({
                where: {
                    id: args.input.id,
                }
            }).catch(() => {
                throw new Error("Prr not found!");
            });

            if (prr.stateStatus === 'Reviewed' && context.user.role !== 'Admin') {
                throw new Error("You can't update PRRs!");
            }

            if (args.input.accountName) {
                prr.accountName = args.input.accountName;
            }

            if (args.input.accountOwner) {
                prr.accountOwner = args.input.accountOwner;
            }

            if (args.input.projectName) {
                prr.projectName = args.input.projectName;
            }

            if (args.input.projectDescription) {
                prr.projectDescription = args.input.projectDescription;
            }

            if (args.input.stage) {
                prr.stage = args.input.stage;
            }

            if (args.input.probability) {
                prr.probability = args.input.probability;
            }

            if (args.input.resolutionStatus) {

                if (context.user.role !== 'Admin') {
                    throw new Error("You can't change resolution status!");
                }

                prr.resolutionStatus = args.input.resolutionStatus;
                // if prr is approved or rejected it means it is reviewed
                prr.stateStatus = "Reviewed";

                // add reviewerId from logged user
                const reviewerId: IGetUserInput = {
                    filter: {
                        id: context.user.id,
                    }
                };
    
                const reviewer = await userResolver.Query.getUser("", reviewerId, context);
                prr.reviewer = reviewer;
            }

            await prr.save();

            return prr;
            
        },

        deletePrr: async (_: unknown, args: IDeletePrrInput, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't delete PRRs!");
            }

            // get all resources from current prr
            const prrId: IGetResourcesInput = {
                filter: {
                    prrId: args.input.id,
                }
            };
            const resources = await resourceResolver.Query.getResources("", prrId, context);

            console.log(resources);

            // delete all resources from current prr
            for (let i = 0; i < resources.length; ++i) {
                const resourceId: IDeleteResourceInput = {
                    input: {
                        id: resources[i].id,
                    }
                }
                const res = await resourceResolver.Mutation.deleteResource("", resourceId, context);
                if (!res) {
                    throw new Error("Something went wrong while deleting resources from this PRR!");
                }
            }

            const res = await getRepository(PrrEntity).delete(args.input.id);

            if (res.affected && res.affected > 0) {
                return true;
            } else {
                return false;
            }
           
        },
    }


}

export default prrResolver;