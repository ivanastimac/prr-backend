import { copyFileSync } from "fs";
import { getRepository } from "typeorm";
import { ResourceEntity } from "../database/entities/ResourceEntity";
import loggedIn from "../middleware/loggedIn";
import { IGetPrrInput } from "../schema/prrSchema";
import { ICreateResourceInput, IDeleteResourceInput, IGetResourceInput, IGetResourcesInput, IUpdateResourceInput } from "../schema/resourceSchema";
import prrResolver from "./prrResolver";
import { IContextUser } from "./userResolver";


const resourceResolver = {

    Query: {

        getResources: async (_: unknown, args: IGetResourcesInput, context: IContextUser) => {

            loggedIn(context);

            const resources = await getRepository(ResourceEntity).find({
                where: {
                    prr: args.filter.prrId,
                }
            });

            return resources;
        },

        getResource: async (_: unknown, args: IGetResourceInput, context: IContextUser) => {

            loggedIn(context);

            const resource = await getRepository(ResourceEntity).findOneOrFail({
                where: {
                    id: args.filter.id,
                }
            }).catch(() => {
                throw new Error("Entity not found!");
            });

            return resource;
        },

    },

    Mutation: {

        createResource: async (_: unknown, args: ICreateResourceInput, context: IContextUser) => {

            loggedIn(context);

            const prrId: IGetPrrInput = {
                filter: {
                    id: args.input.prrId,
                }
            };

            const prr = await prrResolver.Query.getPrr("", prrId, context);

            const newResource = await getRepository(ResourceEntity).create({
                prr: prr,
                resourceLocation: args.input.resourceLocation,
                projectRole: args.input.projectRole,
                internalRole: args.input.internalRole,
                serviceLine: args.input.serviceLine,
                skillSet: args.input.skillSet,
                startDate: args.input.startDate,
                endDate: args.input.endDate,
                allocation: args.input.allocation,
                exampleProfile: args.input.exampleProfile,
            });
            await newResource.save();

            return newResource;
        },

        updateResource: async (_: unknown, args: IUpdateResourceInput, context: IContextUser) => {

            loggedIn(context);

            if (!args.input.resourceLocation && !args.input.projectRole && !args.input.internalRole && !args.input.serviceLine && !args.input.skillSet && !args.input.startDate && !args.input.endDate && !args.input.allocation && !args.input.exampleProfile) {
                throw new Error("You must specify at least one property to update resource's information!");
            }

            const resource = await getRepository(ResourceEntity).findOneOrFail({
                where: {
                    id: args.input.id,
                },
                relations: ["prr"]
            }).catch(() => {
                throw new Error("Entity not found!");
            });

            if (resource.prr.stateStatus === 'Reviewed' && context.user.role !== 'Admin') {
                throw new Error("You can't update resources!");
            }

            if (args.input.resourceLocation) {
                resource.resourceLocation = args.input.resourceLocation;
            }

            if (args.input.projectRole) {
                resource.projectRole = args.input.projectRole;
            }

            if (args.input.internalRole) {
                resource.internalRole = args.input.internalRole;
            }
            
            if (args.input.serviceLine) {
                resource.serviceLine = args.input.serviceLine;
            }

            if (args.input.skillSet) {
                resource.skillSet = args.input.skillSet;
            }

            if (args.input.startDate) {
                resource.startDate = args.input.startDate;
            }

            if (args.input.endDate) {
                resource.endDate = args.input.endDate;
            }

            if (args.input.allocation) {
                resource.allocation = args.input.allocation;
            }

            if (args.input.exampleProfile) {
                resource.exampleProfile = args.input.exampleProfile;
            }

            await resource.save();

            return resource;
        },

        deleteResource: async (_: unknown, args: IDeleteResourceInput, context: IContextUser) => {

            loggedIn(context);

            if (context.user.role !== 'Admin') {
                throw new Error("You can't delete resources!");
            }
            
            const res = await getRepository(ResourceEntity).delete(args.input.id);

            if (res.affected && res.affected > 0) {
                return true;
            } else {
                return false;
            }
           
        },
    }

}

export default resourceResolver;