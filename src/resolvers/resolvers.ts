import prrResolver from "./prrResolver";
import resourceResolver from "./resourceResolver";
import userResolver from "./userResolver";


const resolvers = [userResolver, prrResolver, resourceResolver];

export default resolvers;